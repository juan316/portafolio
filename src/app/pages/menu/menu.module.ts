import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './menu.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path:'',
    component : MenuComponent,
    children:[
      {
        path : 'home',
        loadChildren: () => import('../home/home.module').then( h => h.HomeModule)
      },{
        path : 'info',
        loadChildren: () => import('../sobremi/sobremi.module').then( s => s.SobremiModule)
      },{
        path : 'project',
        loadChildren: () => import('../projects/projects.module').then( p => p.ProjectsModule)
      },{
        path : 'skill',
        loadChildren: () => import('../skills/skills.module').then( s => s.SkillsModule)
      },{
        path : 'jobs',
        loadChildren: () => import('../experience/experience.module').then( e => e.ExperienceModule)
      },{
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  declarations: [MenuComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class MenuModule { }
