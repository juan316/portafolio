import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SobremiComponent } from './sobremi.component';

const routes: Routes = [
  {
    path:'',
    component : SobremiComponent
  }
];

@NgModule({
  declarations: [
    SobremiComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class SobremiModule { }
