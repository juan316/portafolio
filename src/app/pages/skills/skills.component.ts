import { Component, OnInit } from '@angular/core';
import { AppModule } from 'src/app/app.module';
import { UserServiceService } from 'src/app/services/user-service/user-service.service';
import { SkillsService } from 'src/app/services/skills/skills.service';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  constructor(public app : AppModule, 
    private skillService: SkillsService){
    this.app.srcImgBg='fondo3';
  }

  skills:any=[];

  ngOnInit() {
    
    this.skillService.getSkills().then(resp=>{
      let anyData = <any> resp;
      this.skills=anyData.data;
    });

  }
  

}
