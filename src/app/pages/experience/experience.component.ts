import { Component, OnInit } from '@angular/core';
import { AppModule } from 'src/app/app.module';
import { UserServiceService } from 'src/app/services/user-service/user-service.service';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {

  constructor(public app : AppModule,private userService: UserServiceService){
    this.app.srcImgBg='fondo3';
  }

  dataList:any=[];

  ngOnInit() {
     
    this.userService.getExperience().then(resp=>{
      let anyData = <any> resp;
      this.dataList=anyData.data;
    });
  }

}
