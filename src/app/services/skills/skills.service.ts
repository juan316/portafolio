import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AppModule } from 'src/app/app.module';

@Injectable({
  providedIn: 'root'
})
export class SkillsService {
  private configUrl: string='';

  constructor(private http: HttpClient,
    private app : AppModule) { 

  } 
  
  getSkills(){
    return new Promise(resolve => {
      this.http.get(this.app.serviceUrl+'files/skills.json').subscribe({
        next: (v) => {
           //console.log(v)
           resolve(v);
        },
        error: (e) => console.error(e),
        complete: () => console.info('complete') 
      })
    })
  }
}
