import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppModule } from 'src/app/app.module';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(
  public http : HttpClient,
  private app : AppModule
  ) { }

  login(user:any){
    return new Promise(resolve => {
      this.http.post(this.app.serviceUrl+'user/loginUser', user).subscribe((data)=>{
        resolve(data);
      }, error => {
        console.log("Error al iniciar sesión, intente nuevamente o contacte algun administrador");
      })
    })
  }

  getExperience(){
    return new Promise(resolve => {
      this.http.get(this.app.serviceUrl+'files/experience.json').subscribe({
        next: (v) => {
           console.log(v)
           resolve(v);
        },
        error: (e) => console.error(e),
        complete: () => console.info('complete') 
    })
    })
  }

}
